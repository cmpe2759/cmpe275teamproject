﻿Team 17

Jiehan Yao 011938706
Vera Wang  012566476
Rui Li     009304984
Yang Chen  013009243
	

The URL to access your app： http://openhack.thewatercats.com/

Instruction:
   Sometime email will send to spam folder. Please check spam folder as well.
   Some of the api is a little slow due to sending out email. Please be patient.

Build Instruction: 
   Backend: (in server folder)
   * brew services start redis （Mac users)
   * mvn -Dmaven.test.skip=true package  
   * java -jar target/xx.jar
   Frontend: (in client folder)
   * npm install
   * npm start

The GitHub link(we made it public May 21st 11:30pm): https://bitbucket.org/cmpe2759/ 

*Bonus feature: Invite non-members to participate in hackathon event